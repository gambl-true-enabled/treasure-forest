package com.lonteka.montano

import android.content.Context
import android.webkit.JavascriptInterface

private const val FOREST_TABLE = "com.FOREST.table.123"
private const val FOREST_ARGS = "com.FOREST.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(FOREST_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(FOREST_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(FOREST_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(FOREST_ARGS, null)
}
